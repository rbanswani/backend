'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var express = require('express');
var mongoose = require('mongoose');
var cors = require('cors');
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
//DB Connection

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/melon');
app.use(express.static(__dirname + 'public'));
mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('Database connection successful!');
});
app.use(express.static('assets'));
SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  // config swagger ui

  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  app.use(cors())
  var port = process.env.PORT || 10011;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/docs');
  }
});
