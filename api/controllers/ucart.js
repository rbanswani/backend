const userModel = require('../models/auth_Model');
const productModel = require('../models/auth_prod');
const cartModel = require('../models/cartModel');
module.exports = {
    addtoCart: addtoCart,
    listAddToCart:listAddToCart
}

function addtoCart(req, res) {
    var safe = new cartModel();
    console.log("Resp==========", req.body);

    safe.userid = req.body.userid;
    safe.productid = req.body.productid;
    safe.productCost = req.body.productCost;
    safe.productName = req.body.productName;
    safe.quantity = 1;

    cartModel.findOne({
            productid: req.body.productid
        }, function (err, result) {
            if (err) {
                console.log(err);
            } else if (result) {
                console.log(result);
                var Quant = result.quantity + 1

                var totalPrice = Quant * safe.productCost;
                cartModel.findOneAndUpdate({
                    productid: req.body.productid
                }, {
                    $set: {
                        productCost: totalPrice,
                        quantity: Quant
                    }
                }, )
                if (err) {
                    console.log(err);
                } else {
                    res.json({
                        code: 200,
                        message: "successfully",
                        data: result
                    })
                }
            } else {
                safe.save(function (err, response) {
                    if (err) {
                        res.json({
                            code: 404,
                            message: 'Product not added',
                        })
                    } else {
                        res.json({
                            code: 200,
                            data: response,
                        })

                    }
                })
            }
        }

    )
}

function listAddToCart(req, res) {
    console.log('swagger res------', res.swagger)
    console.log(req.swagger.params.id.value);

    cartModel.find({ userid: req.swagger.params.id.value }).sort({ date: 'descending' }).populate({
        path: 'productid',
        model: 'productModel',

    }).exec(function (err, data) {
        // console.log(err)
        console.log("After Listing data", data);
        res.json(data);
    })
}
