
const userModel = require('../models/auth_Model');
const productModel = require('../models/auth_prod');
const jwt = require('jsonwebtoken')
var fs = require('file-system');
module.exports = {
    createuser: createuser,
    adminLogin: adminLogin,
    adduser: adduser,
    listUser: listUser,
    deleteUsers: deleteUsers,
    addproduct: addproduct,
    listproduct: listproduct,
    deleteProduct: deleteProduct,
    editProduct: editProduct,
    editUser: editUser
}

function createuser(req, res) {
    console.log("fsdfsdafasdf", req.body);
    let user = new userModel({
        firstName: req.body.firstName,
        email: req.body.email,
        password: req.body.password,
        is_active: "true",
        is_delete: "false",
        role: "user"

    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            });
            console.log('Successfully created');

        }

    })
}

function adminLogin(req, res) {
    console.log("fdsfdsfdsfdsadmin", req.body);

    var email = req.body.email;
    var password = req.body.password;
    userModel.findOne({
        email: req.body.email,
        is_delete: false
    }, function (err, admin) {
        console.log("admin", admin);
        console.log("fewfwefwefwe", admin);
        if (err) {
            console.log("error");
        }
        else if (admin) {
            const token = jwt.sign(admin.toJSON(), 'ridhi', {
                expiresIn: 1404
            });;
            res.json({
                code: 200,
                message: "success",
                data: admin,
                token: token,

            });
        } else {
            res.json({
                code: 404,
                message: "User not found",
                data: null
            });
        }
        let token = jwt.sign({ foo: 'bar' }, 'hhhhhhh', {

            expiresIn: 1400
        });

        console.log(token);
        //  res.json({ msg: 'Token is created', token: token, data:admin }) 
    });
}
function adduser(req, res) {
    console.log("fsdfsdafasdf", req.body);
    let user = new userModel({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        is_active: "true",
        is_delete: "false",
        role: "user"

    });
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            });
            console.log('Successfully created');

        }

    })

}
function listUser(req, res, next) {
    var useArray = [{
        $project: {
            firstName: '$firstName',
            lastName: '$lastName',
            email: '$email',
        }
    }]
    userModel.aggregate(useArray, function (err, data) {
        console.log("dsfdsfds", data);
        res.json(data);
    })
}
function deleteUsers(req, res, next) {
    var _id = req.swagger.params.id.value;
    console.log(_id);
    userModel.findByIdAndRemove(_id).exec((err, data) => {

        if (err) {
            console.log("error in delete")
        }
        console.log("Data deleted:" + JSON.stringify(data));
    });
}

function addproduct(req, res) {
    console.log(req.swagger.params, " =====================", req.swagger.params.file.originalValue.originalname)

    let file_name = String(Math.random()).substr(2) + '_' + req.swagger.params.file.originalValue.originalname;
    var imagePath = "./assets/images/" + file_name;
    var newFrntendPath = "../frontend/src/assets/images/" + file_name;
    fs.writeFile(newFrntendPath, req.swagger.params.file.originalValue.buffer, function (err) {
        if (err) { throw err }
        console.log(" ALL WELL ", err)
        var mydata = new productModel({
            productName: req.swagger.params.productName.value,
            brandName:  req.swagger.params.brandName.value,
            productCost: req.swagger.params.productCost.value,
            productDescription: req.swagger.params.productDescription.value,
            // shippingAddress: req.swagger.params.shippingAddress.value,
            // phoneNumber: req.swagger.params.phoneNumber.value,
            status: req.swagger.params.status.value,
            file: imagePath
        });
        mydata.save()
            .then(item => {
                console.log("item ====", item)
                res.json({ code: 200, message: 'saved successfully', item });
                // res.json("item saved to database");
            })
            .catch(err => {
                console.log("err", err)
                // res.json({code:500,message:"unable to save to database"});
            })
    })
}




// function addproduct(req, res) {
//     console.log("fsdfsdafasdf", req.body);
//     let owner = new productModel({
//         productName: req.body.productName,
//         customerName: req.body.customerName,
//         productCost: req.body.productCost,
//         shippingCost: req.body.shippingCost,
//         shippingAddress: req.body.shippingAddress,
//         phoneNumber: req.body.phoneNumber,
//         status: req.body.status,
//         file: {type: String},
//     })
//     owner.save(owner, function (err, response) {
//         if (err) {
//             res.json({
//                 code: 404,
//                 message: 'Product not Added'
//             })
//         } else {
//             res.json({
//                 code: 200,
//                 data: response
//             });
//             console.log('Successfully product addedd');
//         }
//     })
// }
function listproduct(req, res, next) {
        productModel.find({}, function (err, data) {
        console.log(" productModel  RESpo =========", data);
        res.json(data);
    })


}
function deleteProduct(req, res, next) {
    var _id = req.swagger.params.id.value;
    console.log(_id);
    productModel.findByIdAndRemove(_id).exec((err, data) => {

        if (err) {
            console.log("error in delete")
        }
        console.log("Product deleted:" + JSON.stringify(data));
    })
}


function editProduct(req, res, next) {
    var timestamp = Date.now();
    var file_name = timestamp + '_'+ req.files.file[0].originalname;
    console.log("Image name", file_name);
    var path = "../frontend/src/assets/images/"+file_name; 
    var retrivingPath =  "./assets/images/"+ file_name;
    fs.writeFile(path,(req.files.file[0].buffer), function (err) {
      if(err) throw err;
      console.log('Image uploaded successfully');
    })
    
    var item = {
        productName: req.swagger.params.productName.value,   
        productCost: req.swagger.params.productCost.value,
        brandName: req.swagger.params.brandName.value,
        
        status: req.swagger.params.status.value,
        file: retrivingPath
    }
    var id = req.swagger.params.id.value
    productModel.findByIdAndUpdate({'_id':id}, {$set: item }).exec((err,data)  => {
    console.log("data updated");
    res.json(data)
    }  )

var id = req.swagger.params.id.value
productModel.findByIdAndUpdate({'_id':id}, {$set: item }).exec((err,data)  => {
console.log("data updated");
res.json(data)
}  )

}



function editUser(req, res, next) {
    var item = req.body;
    console.log(item);
    var id = req.body_id;
    console.log(id)
}
// function addproduct(req,res) {
//     console.log('req.body-->', req.body)
//     var timestamp=Date.now();
//     varimagename = timestamp+ "_" +req.files.file[0].realname;
//     var imagePath= "../frontend/src/assets/images/vada"+varimagename
//     fs.writeFile(imagePath,(req.swagger.params.file.value.buffer),function(err){
//       if(err) {throw err}
//       console.log("img")
//     })
//     var record=new addproducts();
//     record.name=req.swagger.params.name.value;
//     record.productName=req.swagger.params.productName.value;
//     record.customerName=req.swagger.params.customerName.value;
//     record.productCost=req.swagger.params.productCost.value;
//     record.shippingCost=req.swagger.params.shippingCost.value;
//     record.shippingAddress=req.swagger.params.shippingAddress.value;
//     record.phoneNumber=req.swagger.params.phoneNumber.value;
//     record.status=req.swagger.params.status.value;
//     record.file="assests/images/vada"+ varimagename;
//     record.save(function (err,data){
//       console.log('product image')
//       res.send({code:200,message:'product image add', data:data});
//     })
//   }

//  function fileUpload(req, res) {
//     console.log('req.body-->',req.body)
//     console.log('req.body-->',req.body) 
//     var timestamp=Date.now();
//     varimageName = timestamp+ "_" +req.files.file[0].realname;
//     var imagePath = "upload/"+varimageName;
//     fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
//         if(err) {throw err}
//         console.log("img")
//     })
// var record=new auth_prod();
// record.productName=req.swagger.params.productName.value;
// record.customerName=req.swagger.params.customerName.value;
// record.productCost=req.swagger.params.productCost.value;
// record.shippingCost=req.swagger.params.shippingCost.value;
// record.shippingAddress=req.swagger.params.shippingAddress.value;
// record.phoneNumber=req.swagger.params.phoneNumber.value;
// record.status=req.swagger.params.status.value;
// record.file="upload/"+ varimageName;
// record.save(function (err,data){
//     console.log('blog')
//     res.send({code:200,message:'product add',data:data});
// })
// }