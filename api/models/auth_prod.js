const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AdminSchema = new mongoose.Schema({
    brandName: {type: String},
    productName: { type: String },
    // customerName:{type: String},
    productCost:{type: Number},
    productDescription:{type: String},
    // shippingAddress: {type: String},    
    // phoneNumber:{type: String},
    status: {type: String},
    file: {type: String},
    is_delete:{type: Boolean,default:false}

});
const productModel = mongoose.model('productModel', AdminSchema);
module.exports = productModel;
