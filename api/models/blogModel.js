const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let blogSchema = new mongoose.Schema({
    blogtitle: { type: String },
    blogcontent:{type: String},
   file: {type: String},
   date: {type: Date},
   is_delete:{type: Boolean,default:false}
   
});
const blogModel = mongoose.model('blogModel', blogSchema);
module.exports = blogModel;
