const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let CommonSchema = new mongoose.Schema({
    userid: { type: String,ref:'UserModel' },
    blogid: { type: String,ref:'blogModel' },
    comment: {type: String},
    date: {type: Date},
    ratings: {type: Number}

});
const commonModel = mongoose.model('commonModel', CommonSchema);
module.exports = commonModel;