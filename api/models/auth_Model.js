const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let UserSchema = new mongoose.Schema({
    firstName: { type: String },
    lastName:{type: String},
    email: { type: String },
    password: { type: String },
    role: { type: String },
    is_active: { type: Boolean },
    is_delete: { type: Boolean },
   
});
const userModel = mongoose.model('UserModel', UserSchema);
module.exports = userModel;
