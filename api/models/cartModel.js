const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let cartSchema = new mongoose.Schema({
    userid: {type: String,ref:'UserModel'},
    productid:{type: String,ref:'productModel'},
    productName: {type: String,ref:'productModel'},
   quantity: {type: Number, default:1},
   date: {type:String, default:Date.now()},
   productCost:Number
   
});
const cartModel = mongoose.model('cartModel', cartSchema);
module.exports = cartModel;
